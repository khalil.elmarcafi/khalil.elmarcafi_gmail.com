import random
import csv
import pandas as pd
import time
from pandas import DataFrame
from pandas.io.parsers import TextFileReader
from Joueur import *
from typing import List, Any


menu_options = {
    1: 'User creation',
    2: 'Play',
    3: 'Instructions',
    4: 'Exit'
}

def print_menu():
    for key in menu_options.keys():
        print (key, '--', menu_options[key] )

def gamble(joueur):
    if int(joueur.getsolde()) == 0:
        return ('Votre solde est nul, vous ne pouvez pas jouer, veuillez recharger votre solde.')
        exit()
    else:
        print('Veuillez entrer le montant du pari')
        MntPari = int(input())
        while int(joueur.getsolde()) - MntPari < 0:
            print('Opération impossible, veuillez renseigner un montant de pari inférieur à votre solde')
            MntPari = int(input())
        joueur.setsolde(joueur.getsolde() - MntPari)
        print('Veuillez entrer un chiffre positif situé entre 1 et 10')
        UtilisateurValue = int(input())
        RandomValue = random.randrange(1, 11)
        i = 1
        Gain = 0
        while i < 2:
            if int(UtilisateurValue) == RandomValue and i == 1:
                Gain = MntPari * 3
                print('Félicitations, vous avez remporté le jackpot, vous encaissez', Gain)
                break
            else:
                i = i + 1
                print(
                    'Vous n''avez pas gagné, vous avez une deuxième change, veuillez entrer un chiffre positif situé entre 1 et 10')
                UtilisateurValue = int(input())
                if UtilisateurValue == RandomValue and i == 2:
                    Gain = MntPari * 1.2
                    print('Félicitations, vous avez gagné, vous encaissez', Gain)
                    break
                else:
                    Gain = 0
                    print('La chance sourit aux audacieux, ne jamais baisser les bras')
        if Gain == 0:
            print('Le numéro gagnant est le : ', RandomValue)
            print('Votre solde est de : ', int(joueur.getsolde()))
        else:
            print('Votre nouveau solde est de xxxx:', int(joueur.getsolde())+Gain)
        joueur.setsolde(int(joueur.getsolde())+Gain)

def create_account():
    print('Première connexion, créez votre compte, utilisez le code suivant pour obtenir 30 euros : SIEE243')
    nom = input("Veuillez renseigner votre nom")
    prenom = input("Veuillez renseigner votre prénom")
    pseudo = input("Veuillez renseigner votre pseudo")
    age = input("Veuillez renseigner votre age")
    if int(age) < 18:
        print("Attendez d'avoir 18 ans et revenez ! À bientôt")
        exit()
    email = input("Veuillez renseigner votre email")
    phone = input("Veuillez renseigner votre phone")
    Cdepromo = input('Veuillez renseigner le code promo')
    if Cdepromo == 'SIEE243':
        solde = 30
    else:
        solde = 0
    password = input("Veuillez renseigner votre code secret")
    joueur = Joueur(nom, prenom, pseudo, age, email, phone, solde, password)
    with open(filename, 'a') as csvfile1:
        csvwriter1 = csv.writer(csvfile1)
        l = [[joueur.getunique()]+joueur.getinfosjoueur()]
        csvwriter1.writerows(l)
        csvfile1.close()

def connection():
    print('Bienvenue sur TheGamblingPlanet')
    email = input("Veuillez renseigner votre adresse mail")
    passwd = input("Veuillez renseigner votre code secret")
    df: TextFileReader | DataFrame | Any = pd.read_csv('playerslist.csv',delimiter=',')
    finder = df[(df['ID'] == email+'_'+passwd)]
    return [finder,email+'_'+passwd]

def instructions():
     print("L'objectif du jeu est de deviner un chiffre de 1 à 10.")
     print("Deux cas de figures existe : ")
     print("1- Gagner au bout de la première tentative = Montant du pari * 3")
     print("2- Gagner au bout de la première tentative = Montant du pari * 1.2")

     print('Bonne chance !')
if __name__ == '__main__':
    filename = "playerslist.csv"
    while(True):
        print("Bienvenue sur The Gambling Planet")
        print("Pour la démonstration, veuillez utiliser comme email : Ka et comme mot de passe : P")
        print_menu()
        option = ''
        try:
            option = int(input('Entrez votre choix: '))
        except:
            print('------------------------------------------------')

        if option == 1:
            create_account()
        elif option == 2:
            [finder,pw] = connection()
            if len(finder) == 0:
                print('Ce compte n''existe pas ou les identifiants que vous avez saisis sont incorrects')
                exit()
            infos_user = finder.to_dict('Records')[0]
            j1=Joueur(infos_user['Nom'],infos_user['Prenom'], infos_user['Pseudo'], infos_user['Age'], str(infos_user['Email']), infos_user['Phone'], infos_user['Solde'], str(infos_user['ID']))
            print("Bienvenue", j1.getnom(),"",j1.getprenom())
            print("Votre solde est de : ", j1.getsolde())
            if j1.getsolde()>0:
                gamble(j1)
            else:
                print("Si vous souhaitez recharger votre solde, appuyez sur 1")
                l =int(input())
                if l == 1:
                    print("Introduisez le montant souhaité")
                    mnt_r = int(input())
                    print("Introduisez votre RIB")
                    RIB = input()
                    while (len(RIB)!=34):
                        print("Selon les normes un RIB contient 34 caractères ! Merci de réessayer ")
                        print("Introduisez le RIB")
                        RIB = input()
                    else:
                        print("Vérification")
                        for i in range(0,5):
                            print(".")
                            time.sleep(0.5)
                        print("Opération en cours")
                        time.sleep(3)
                        print("Opération effectuée")
                    j1.setsolde(j1.getsolde() + mnt_r)
                    df = pd.read_csv('playerslist.csv')
                    index = df.index[df['ID'] == pw]
                    df.at[int(index.values), 'Solde'] = j1.getsolde()
                    df.to_csv('playerslist.csv', index=False)
                else:
                    exit()
            df = pd.read_csv('playerslist.csv')
            index = df.index[df['ID'] == pw]
            df.at[int(index.values) ,'Solde'] = j1.getsolde()
            df.to_csv('playerslist.csv',index=False)
        elif option == 4:
            print('Au revoir et à bientôt')
            exit()
        elif option == 3:
            instructions()
        else:
            print('Entrée invalide, introduisez un nombre entre 1 et 4.')








