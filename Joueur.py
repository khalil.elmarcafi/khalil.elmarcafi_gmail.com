class Joueur:


    def __init__(self, nom, prenom, pseudo, age, email, phone, solde, password):
        self.nom = nom
        self.prenom = prenom
        self.pseudo = pseudo
        self.age = age
        self.email = email
        self.unique = email+"_"+password
        self.phone = phone
        self.solde = solde
        self.password = password

    def getnom(self):
        return self.nom

    def getprenom(self):
        return self.prenom

    def getpseudo(self):
        return self.pseudo

    def getage(self):
        return self.age

    def getemail(self):
        return self.email

    def getphone(self):
        return self.phone

    def getsolde(self):
        return int(self.solde)

    def getunique(self):
        return self.unique

    def getinfosjoueur(self):
        return list([self.nom ,  self.prenom , self.pseudo ,  self.age ,  self.email , self.phone , self.solde])

    def setsolde(self, Gain):
        self.solde = Gain
